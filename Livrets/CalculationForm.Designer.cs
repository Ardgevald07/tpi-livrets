﻿namespace Livrets
{
    partial class CalculationForm
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCalculation = new System.Windows.Forms.Label();
            this.panelCalculusTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.labelRemainingTime = new System.Windows.Forms.Label();
            this.labelNumberCalculus = new System.Windows.Forms.Label();
            this.textBoxAnswer = new System.Windows.Forms.TextBox();
            this.panelCalculusTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelCalculation
            // 
            this.labelCalculation.AutoSize = true;
            this.labelCalculation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCalculation.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold);
            this.labelCalculation.Location = new System.Drawing.Point(3, 0);
            this.labelCalculation.Name = "labelCalculation";
            this.labelCalculation.Size = new System.Drawing.Size(470, 240);
            this.labelCalculation.TabIndex = 0;
            this.labelCalculation.Text = "10   x   15   =";
            this.labelCalculation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelCalculusTableLayout
            // 
            this.panelCalculusTableLayout.ColumnCount = 2;
            this.panelCalculusTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.panelCalculusTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.panelCalculusTableLayout.Controls.Add(this.labelCalculation, 0, 0);
            this.panelCalculusTableLayout.Controls.Add(this.labelRemainingTime, 0, 1);
            this.panelCalculusTableLayout.Controls.Add(this.labelNumberCalculus, 0, 2);
            this.panelCalculusTableLayout.Controls.Add(this.textBoxAnswer, 1, 0);
            this.panelCalculusTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCalculusTableLayout.Location = new System.Drawing.Point(0, 0);
            this.panelCalculusTableLayout.Name = "panelCalculusTableLayout";
            this.panelCalculusTableLayout.RowCount = 3;
            this.panelCalculusTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.panelCalculusTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.panelCalculusTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.panelCalculusTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.panelCalculusTableLayout.Size = new System.Drawing.Size(680, 400);
            this.panelCalculusTableLayout.TabIndex = 7;
            // 
            // labelRemainingTime
            // 
            this.labelRemainingTime.AutoSize = true;
            this.panelCalculusTableLayout.SetColumnSpan(this.labelRemainingTime, 2);
            this.labelRemainingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRemainingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.labelRemainingTime.Location = new System.Drawing.Point(3, 240);
            this.labelRemainingTime.Name = "labelRemainingTime";
            this.labelRemainingTime.Size = new System.Drawing.Size(674, 80);
            this.labelRemainingTime.TabIndex = 1;
            this.labelRemainingTime.Text = "Temps restant : 10 s";
            this.labelRemainingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNumberCalculus
            // 
            this.labelNumberCalculus.AutoSize = true;
            this.panelCalculusTableLayout.SetColumnSpan(this.labelNumberCalculus, 2);
            this.labelNumberCalculus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNumberCalculus.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labelNumberCalculus.Location = new System.Drawing.Point(3, 320);
            this.labelNumberCalculus.Name = "labelNumberCalculus";
            this.labelNumberCalculus.Size = new System.Drawing.Size(674, 80);
            this.labelNumberCalculus.TabIndex = 2;
            this.labelNumberCalculus.Text = "9 / 15";
            this.labelNumberCalculus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxAnswer
            // 
            this.textBoxAnswer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F);
            this.textBoxAnswer.Location = new System.Drawing.Point(496, 80);
            this.textBoxAnswer.Margin = new System.Windows.Forms.Padding(20);
            this.textBoxAnswer.MaxLength = 3;
            this.textBoxAnswer.Name = "textBoxAnswer";
            this.textBoxAnswer.Size = new System.Drawing.Size(164, 80);
            this.textBoxAnswer.TabIndex = 3;
            this.textBoxAnswer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CalculationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.panelCalculusTableLayout);
            this.Name = "CalculationForm";
            this.Size = new System.Drawing.Size(680, 400);
            this.panelCalculusTableLayout.ResumeLayout(false);
            this.panelCalculusTableLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelCalculation;
        private System.Windows.Forms.TableLayoutPanel panelCalculusTableLayout;
        private System.Windows.Forms.Label labelRemainingTime;
        private System.Windows.Forms.Label labelNumberCalculus;
        private System.Windows.Forms.TextBox textBoxAnswer;

    }
}
