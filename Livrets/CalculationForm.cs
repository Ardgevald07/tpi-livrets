﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : User Control to the display of the questions and the answers
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System.Windows.Forms;

namespace Livrets
{
    public partial class CalculationForm : UserControl
    {
        CalculationManager calculationManager;

        /// <summary>
        /// constructor of the class with the calculationManager's instance
        /// </summary>
        /// <param name="calculationManager">instance of the calculationManager</param>
        public CalculationForm(CalculationManager calculationManager)
        {
            InitializeComponent();
            this.calculationManager = calculationManager;
            textBoxAnswer.KeyDown += new KeyEventHandler(calculationManager.textBoxAnswer_keyDown);
        }

        /// <summary>
        /// Set the calculus label and the step label
        /// </summary>
        /// <param name="calculusText">text of the calculus</param>
        /// <param name="stepText">text of the step we are at</param>
        public void SetLabelsCalculus(string calculusText, string stepText)
        {
            // delegate if different thread
            if (this.labelCalculation.InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate()
                {
                    labelCalculation.Text = calculusText;
                    labelNumberCalculus.Text = stepText;
                });
            }
            else
            {
                labelCalculation.Text = calculusText;
                labelNumberCalculus.Text = stepText;
            }
        }

        /// <summary>
        /// Sets the remaining time (each second)
        /// </summary>
        /// <param name="remainingTime">time remaining (already formatted)</param>
        public void SetRemainingTime(string remainingTime)
        {
            // delegate if different thread
            if (this.labelRemainingTime.InvokeRequired)
            {

                labelRemainingTime.BeginInvoke((MethodInvoker)delegate()
                {
                    labelRemainingTime.Text = remainingTime;
                });
            }
            else
            {
                labelRemainingTime.Text = remainingTime;
            }
        }

        /// <summary>
        /// empty and focus on textbox at next question
        /// </summary>
        public void resetAnswer()
        {
            labelCalculation.BeginInvoke((MethodInvoker)delegate()
            {
                textBoxAnswer.Text = "";
                textBoxAnswer.Focus();
            });
        }
    }//end CalculationForm
}//end namespace
