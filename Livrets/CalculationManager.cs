﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : Manage the application calculations, and calls the display classes
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Windows.Forms;

namespace Livrets
{
    /// <summary>
    /// Class containing all of the calculation management
    /// </summary>
    public class CalculationManager
    {
        //////////////// CONSTANTS //////////////////
        //----------- Constants defining the default values ---------------
        const int DEFAULT_MODE = 1;

        const int DEFAULT_CALCULUS_NUMBER = 10;
        const int DEFAULT_TIME_SECONDS = 10;

        const int MIN_FACTOR = 1;
        const int MAX_FACTOR = 15;

        const int TIMER_INTERVAL = 10;

        const int CALCULATION_INIT = -1; //must be -1 because we increment it before using it

        //----------- String formats to display ---------------
        const string FORMAT_MODE_1 = "{0:D}   x   {1:D}   =   ____";
        const string FORMAT_MODE_2 = "{0:D}   x   ____    =   {1:D}";
        const string FORMAT_MODE_3 = "{0:D}   x   {1:D}   =";

        public const string FORMAT_STOPWATCH = "{0:00}:{1:00}.{2:00}";

        const string FORMAT_REMAINING_TIME = "Temps restant : {0} s";

        //----------- Names of the elements used ---------------
        const string CONTAINER_PANEL_NAME = "panelMainContainer";
        const string LABEL_TIMER_NAME = "labelTimer";
        const string BUTTON_OPTIONS_NAME = "buttonOptions";
        const string BUTTON_STOP_NAME = "buttonStop";

        //----------- default texts ---------------
        const string DEFAULT_TIMER_LABEL = "00:00.00";

        const string TEXT_IF_NO_ANSWER = "____";

        /////////////// VARIABLES /////////////////
        //----------- Form objects ---------------
        private Form mainForm;
        
        private CalculationDisplay calculationDisplay;
        private CalculationForm calculationForm;
        private AnswersDisplay answerDisplay;

        private Panel panelMainContainer;

        private Label labelTimer;
        private Button buttonOptions;
        private Button buttonStop;

        //----------- Time Management ---------------
        private Stopwatch stopwatch;
        private System.Timers.Timer timer;

        private int calculationStep;
        private TimeSpan elapsedTime;
        private TimeSpan calculationStepTime;
        private TimeSpan secondsElapsed;

        //--------  application Options ---------
        public int optionMode { get; set; }
        public int[] factors { get; set; }

        public int calculationNumber { get; set; }
        public int playTime { get; set; }

        // declared at instantiation of the class to maintain the same seed
        // for random numbers
        private Random randomNumber = new Random();

        // Contains the calculations with the results
        private Calculation[] calculations;

        private string textFormat;

        /// <summary>
        /// Constructor of the instance called by the createInstance function
        /// </summary>
        /// <param name="sender">Element containing all the internal UI</param>
        public CalculationManager(MainWindow sender)
        {
            // initialisations
            optionMode = DEFAULT_MODE;
            calculationNumber = DEFAULT_CALCULUS_NUMBER;
            playTime = DEFAULT_TIME_SECONDS;
            calculationStep = CALCULATION_INIT;
            factors = new int[MAX_FACTOR];

            for (int i = 0; i < MAX_FACTOR; i++)
            {
                factors[i] = i + 1;
            }

            // form from where this class is called
            mainForm = sender;

            // elements from the mainForm to get
            panelMainContainer = mainForm.Controls.Find(CONTAINER_PANEL_NAME, true)[0] as Panel;

            labelTimer = mainForm.Controls.Find(LABEL_TIMER_NAME, true)[0] as Label;

            buttonOptions = mainForm.Controls.Find(BUTTON_OPTIONS_NAME, true)[0] as Button;
            buttonStop = mainForm.Controls.Find(BUTTON_STOP_NAME, true)[0] as Button;

            //time management with a system timer and a stopwatch
            timer = new System.Timers.Timer();
            stopwatch = new Stopwatch();

            timer.Elapsed += new ElapsedEventHandler(showNextOnTimer);
            timer.Interval = TIMER_INTERVAL; // in miliseconds
        }

        /////////////// Events /////////////////

        /// <summary>
        /// Display a calculation when time is depleted (event called)
        /// </summary>
        /// <param name="source">source object calling this event</param>
        /// <param name="e">Event variable for the timer</param>
        private void showNextOnTimer(object source, ElapsedEventArgs e)
        {
            elapsedTime = stopwatch.Elapsed;

            // Change label with delegation (we are in a different thread)
            labelTimer.BeginInvoke((MethodInvoker)delegate ()
            {
                labelTimer.Text = String.Format(FORMAT_STOPWATCH,
                    elapsedTime.Minutes,
                    elapsedTime.Seconds,
                    elapsedTime.Milliseconds / 10
                );
            });

            // pass onto next when time is depleted
            if (elapsedTime.TotalSeconds - calculationStepTime.TotalSeconds > playTime)
            {
                saveAnswer(TEXT_IF_NO_ANSWER, elapsedTime);
                calculationStepTime = elapsedTime;
            }

            // display remaining time each second
            if (elapsedTime.TotalSeconds - secondsElapsed.TotalSeconds > 1)
            {
                updateRemainingTime(playTime - (int)(elapsedTime.TotalSeconds - calculationStepTime.TotalSeconds));
                secondsElapsed = elapsedTime;
            }
        }



        /// <summary>
        /// when "Enter" is pressed in the textbox of the calculationForm
        /// save the answer and go to next calculus
        /// </summary>
        /// <param name="sender">trigger of the event</param>
        /// <param name="e">the args for the pressed key</param>
        public void textBoxAnswer_keyDown(object sender, KeyEventArgs e)
        {
            TextBox textBoxAnswer = sender as TextBox;

            // we reset the timer for the next answer
            if (e.KeyCode == Keys.Enter)
            {
                // if answer empty, put replacement text instead
                saveAnswer(
                    (textBoxAnswer.Text == "" ? TEXT_IF_NO_ANSWER : textBoxAnswer.Text)
                    , stopwatch.Elapsed
                );

                calculationStepTime = stopwatch.Elapsed;
                secondsElapsed = stopwatch.Elapsed;
            }
        }

        /// <summary>
        /// Create a serie of calculations based on parameters (factors, number)
        /// </summary>
        private void generateCalculation()
        {
            // use the Calculation object to have a easy access to values and results
            this.calculations = new Calculation[calculationNumber];

            for (int i = 0; i < calculationNumber; i++)
            {
                Calculation calculation = new Calculation(
                    randomNumber.Next(MIN_FACTOR, MAX_FACTOR + 1),
                    factors[randomNumber.Next(factors.Length)]
                );

                calculations[i] = calculation;
            }
        }

        /// <summary>
        /// Begin a calculation serie
        /// </summary>
        public void startCalculation()
        {
            // initialisation and clearing
            calculationStep = CALCULATION_INIT;

            calculationStepTime = TimeSpan.Zero;
            secondsElapsed = TimeSpan.Zero;

            panelMainContainer.Controls.Clear();
            labelTimer.Text = DEFAULT_TIMER_LABEL;

            // the result is in calculations
            generateCalculation();

            // setting the method for the different modes
            switch (optionMode)
            {
                case 1:
                    panelMainContainer.Controls.Add(calculationDisplay = new CalculationDisplay());

                    textFormat = FORMAT_MODE_1;
                    break;
                case 2:
                    panelMainContainer.Controls.Add(calculationDisplay = new CalculationDisplay());

                    textFormat = FORMAT_MODE_2;
                    break;
                case 3:
                    panelMainContainer.Controls.Add(calculationForm = new CalculationForm(this));

                    textFormat = FORMAT_MODE_3;
                    break;
                default:
                    break;
            }

            // change buttons usable (no options, but can stop)
            buttonOptions.Enabled = false;
            buttonStop.Visible = true;

            // the timer manage the play with the showNextOnTimer event
            showNextCalculation();
            timer.Start();
            stopwatch.Start();
        }// end startCalculation

        /// <summary>
        /// puts an end to a play
        /// the results are all displayed, even if they weren't showed once
        /// </summary>
        public void stopCalculation()
        {
            // stop any timer event to run
            timer.Stop();
            stopwatch.Stop();
            stopwatch.Reset();

            // delegation if it stops with timer
            if(buttonOptions.InvokeRequired || buttonStop.InvokeRequired)
            {
                buttonOptions.BeginInvoke((MethodInvoker)delegate ()
                {
                    // change the usable buttons
                    buttonOptions.Enabled = true;
                    buttonStop.Visible = false;
                });
            }
            else
            { 
                // change the usable buttons
                buttonOptions.Enabled = true;
                buttonStop.Visible = false;
            }
            
            // show the results
            showResult();
        }

        /// <summary>
        /// Display the remaining time in the corresponding window
        /// </summary>
        /// <param name="remainingTime">Time remaining to display</param>
        private void updateRemainingTime(int remainingTime)
        {
            string remainingTimeText = String.Format(FORMAT_REMAINING_TIME, remainingTime);

            switch (optionMode)
            {
                case 1:
                    calculationDisplay.SetRemainingTime(remainingTimeText);
                    break;
                case 2:
                    calculationDisplay.SetRemainingTime(remainingTimeText);
                    break;
                case 3:
                    calculationForm.SetRemainingTime(remainingTimeText);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// show the different calculations
        /// </summary>
        private void showNextCalculation()
        {
            // set the correct remaining time at each start
            updateRemainingTime(playTime);

            calculationStep++;

            // Keep going until we do all the calculations
            if (calculationStep < calculationNumber)
            {
                string calculationText;
                int baseFactor = calculations[calculationStep].baseFactor;
                int tableFactor = calculations[calculationStep].tableFactor;

                // Show at which step we are
                string stepText = (calculationStep + 1) + " / " + calculationNumber;

                switch (optionMode)
                {
                    case 1:
                        calculationText = String.Format(textFormat, baseFactor, tableFactor);
                        calculationDisplay.SetLabelsCalculus(calculationText, stepText);
                        break;

                    case 2:
                        calculationText = String.Format(textFormat, baseFactor, baseFactor * tableFactor);
                        calculationDisplay.SetLabelsCalculus(calculationText, stepText);
                        break;
                    case 3:
                        calculationText = String.Format(textFormat, baseFactor, tableFactor);
                        calculationForm.SetLabelsCalculus(calculationText, stepText);

                        calculationForm.resetAnswer();
                        break;
                    default:
                        break;  
                }
            }
            else
            {
                // the timer reset at each Stop
                stopCalculation();
                
            }
        }// end showNextCalculation

        /// <summary>
        /// save the answer and the time
        /// </summary>
        /// <param name="answer">the answer to save</param>
        /// <param name="answer">time the answer was set (from start) </param>
        private void saveAnswer(string answer, TimeSpan answerTime)
        {
            calculations[calculationStep].answer = answer;
            calculations[calculationStep].time = answerTime - calculationStepTime;

            showNextCalculation();
        }

        /// <summary>
        /// display the results of the calculations
        /// </summary>
        private void showResult()
        {
            // Delegation to change panel in another thread
            panelMainContainer.BeginInvoke((MethodInvoker)delegate()
            {
                panelMainContainer.Controls.Clear();
                panelMainContainer.Controls.Add(answerDisplay = new AnswersDisplay(this));

                if(optionMode <= 2){
                    answerDisplay.setAnswers(calculations);
                }
                else{

                    // split the array by true and false to sort each part separatly
                    List<Calculation> calculationTrue = new List<Calculation>();
                    List<Calculation> calculationFalse = new List<Calculation>();

                    foreach (Calculation calculation in calculations) 
                    {
                        switch (calculation.isCorrect)
                        {
                            case true :
                                calculationTrue.Add(calculation);
                                break;
                            case false :
                                calculationFalse.Add(calculation);
                                break;
                        }
                    }

                    // sort in reverse order
                    calculationTrue.Sort();
                    calculationTrue.Reverse();
                    calculationFalse.Sort();
                    calculationFalse.Reverse();

                    calculations = calculationFalse.Union(calculationTrue).ToArray();

                    answerDisplay.SetAnswersDetailled(calculations);
                }
            });
        }

    }// end class CalculationManager
}//end namespace
