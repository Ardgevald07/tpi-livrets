﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : Main window of the application
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System;
using System.Windows.Forms;

namespace Livrets
{
    public partial class MainWindow : Form
    {
        CalculationManager calculationManager;

        /// <summary>
        /// constructor of the class (main class)
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            calculationManager = new CalculationManager(this);
        }

        /// <summary>
        /// starts a serie of calculations
        /// </summary>
        /// <param name="sender">button clicked (mandatory)</param>
        /// <param name="e">event arguments (mandatory)</param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            calculationManager.startCalculation();
        }

        /// <summary>
        /// opens the popup to change the options
        /// </summary>
        /// <param name="sender">button clicked (mandatory)</param>
        /// <param name="e">event arguments (mandatory)</param>
        private void buttonOptions_Click(object sender, EventArgs e)
        {
            // use the OptionsPopup form as popup
            OptionsPopup optionsPopup = new OptionsPopup(calculationManager);
            DialogResult optionsValidation = optionsPopup.ShowDialog();

            // waits for the popup to be closed before going on
            if (optionsValidation == DialogResult.OK)
            {
                labelMode.Text = "Mode " + calculationManager.optionMode;
            }

            optionsPopup.Dispose();
        }

        /// <summary>
        /// stops any calculation and display the results
        /// </summary>
        /// <param name="sender">button clicked (mandatory)</param>
        /// <param name="e">event arguments (mandatory)</param>
        private void buttonStop_Click(object sender, EventArgs e)
        {
            calculationManager.stopCalculation();
        }
    }//end class MainWindow
}//end namespace
