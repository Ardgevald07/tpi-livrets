﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : window to change the parameters of the application
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Livrets
{
    public partial class OptionsPopup : Form
    {
        // to check if at least one checkbox is checked
        int numberFactors;

        CalculationManager calculationManager;

        /// <summary>
        /// Class constructor
        /// Get the options in the calculationManager to display on the options popup
        /// </summary>
        public OptionsPopup(CalculationManager calculationManager)
        {
            InitializeComponent();

            this.calculationManager = calculationManager;

            switch (calculationManager.optionMode)
            {
                case 1 :
                    radioButtonMode1.Checked = true;
                    break;
                case 2:
                    radioButtonMode2.Checked = true;
                    break;
                case 3:
                    radioButtonMode3.Checked = true;
                    break;
            }

            numberFactors = calculationManager.factors.Length;

            foreach (CheckBox factorButton in groupBoxTable.Controls)
            {
                if (calculationManager.factors.Contains(getNameNumber(factorButton.Name)))
                {
                    factorButton.Checked = true;
                }

                factorButton.CheckedChanged += new EventHandler(checkIfAny);
            }

            numericUpDownNbCalculus.Value = calculationManager.calculationNumber;
            numericUpDownDisplayTime.Value = calculationManager.playTime;
        }// end OptionPopup

        /// <summary>
        /// Event triggering each time a factor checkbox is changed
        /// Disable the validation button if ther is no factor selected
        /// </summary>
        /// <param name="sender">The changed checkbox </param>
        /// <param name="e">the event itself</param>
        private void checkIfAny(object sender, EventArgs e)
        {
            CheckBox boxChecked = (CheckBox)sender;
            if (boxChecked.Checked)
            {
                numberFactors++;
                buttonConfirmOptions.Enabled = true;
            }
            else
            {
                numberFactors--;
                if (numberFactors == 0)
                {
                    buttonConfirmOptions.Enabled = false;
                }
            }
        }

        /// <summary>
        /// change the parameters of the application
        /// </summary>
        /// <param name="sender">sen</param>
        /// <param name="e"></param>
        private void buttonConfirmOptions_Click(object sender, EventArgs e)
        {
            // get the checked button in the group
            RadioButton checkedButton = groupBoxMode.Controls.OfType<RadioButton>().FirstOrDefault(radioButton => radioButton.Checked);
            calculationManager.optionMode = getNameNumber(checkedButton.Name);

            List<int> factors = new List<int>();

            foreach (CheckBox factorButton in groupBoxTable.Controls)
            {
                if (factorButton.Checked)
                {
                    factors.Add(getNameNumber(factorButton.Name));
                }
            }
            calculationManager.factors = factors.ToArray();

            calculationManager.calculationNumber = Convert.ToInt16(numericUpDownNbCalculus.Value);
            calculationManager.playTime = Convert.ToInt16(numericUpDownDisplayTime.Value);
        }

        /// <summary>
        /// Get the number in the name of a variable (or any text)
        /// Must only be used if there is a number
        /// </summary>
        /// <param name="variableName">The variable Name containing a number</param>
        /// <returns>The number we were looking for</returns>
        /// <exception cref="ArgumentException">If the variable does not contain a number</exception>
        private int getNameNumber(string variableName)
        {
            // Regex to get only the number
            Regex regexGetNumber = new Regex(@"\d+");
            Match match = regexGetNumber.Match(variableName);

            if (match.Success)
            {
                return Convert.ToInt16(match.Value);
            }
            else
            {
                // Exception for misuse of the function
                throw new ArgumentException("The variable must contain a number", variableName);
            }
        }
    }// end Class
}//end namespace
