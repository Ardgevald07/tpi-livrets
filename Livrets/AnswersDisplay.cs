﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : User Control to the display of the answers
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Livrets
{
    public partial class AnswersDisplay : UserControl
    {
        CalculationManager calculationManager;
        TableLayoutControlCollection panelControls;

        /// <summary>
        /// constructor of this class
        /// </summary>
        /// <param name="calculationManager">instance of the calculatiomManager</param>
        public AnswersDisplay(CalculationManager calculationManager)
        {
            InitializeComponent();
            this.calculationManager = calculationManager;
            panelControls = panelAnswersTalbeLayout.Controls;
        }

        /// <summary>
        /// Display the answers for mode 1 and 2
        /// </summary>
        /// <param name="calculations">the array of calculations to display</param>
        public void setAnswers(Calculation[] calculations){
            
            int i = 1;
            Color tableFactorColor = Color.Black;
            Color answerColor = Color.Green;

            foreach (Calculation calculation in calculations)
            {
                string calculationNumber = String.Format("{0})",i++);
                string calculationBaseFactor = calculation.baseFactor.ToString();
                string calculationTableFactor = calculation.tableFactor.ToString();
                string calculationCorrectAnswer = (calculation.baseFactor * calculation.tableFactor).ToString();

                if (calculationManager.optionMode == 2)
                {
                    tableFactorColor = Color.Green;
                    answerColor = Color.Black;
                }


                createlabelAnswer(calculationNumber);
                createlabelAnswer(calculationBaseFactor);
                createlabelAnswer("x");
                createlabelAnswer(calculationTableFactor, tableFactorColor);
                createlabelAnswer("=");
                createlabelAnswer(calculationCorrectAnswer, answerColor);
            }
        }

        /// <summary>
        /// Display the answers for mode 3
        /// </summary>
        /// <param name="calculations">the array of calculus to display</param>
        public void SetAnswersDetailled(Calculation[] calculations)
        {

            // formats to use for display
            const string ANSWER_FORMAT = "Réponse : {0}";
            const string TIME_TEXT_FORMAT = "Temps : ";
            const string QUESTION_FORMAT = "{0,-3}  x  {1,-3}   ";

            bool previousResult = true;

            foreach (Calculation calculation in calculations)
            {
                // separation between false and true answers (empty line)
                if (calculation.isCorrect && !previousResult)
                {
                    createlabelAnswer("",12);
                }

                previousResult = calculation.isCorrect;

                // formatting of the different elements
                string calculationQuestion = String.Format(QUESTION_FORMAT, calculation.baseFactor, calculation.tableFactor);
                string calculationAnswer = calculation.answer;
                string calculationCorrectAnswer = String.Format(ANSWER_FORMAT,calculation.baseFactor * calculation.tableFactor);
                string calculationTime = String.Format(
                    TIME_TEXT_FORMAT + CalculationManager.FORMAT_STOPWATCH,
                    calculation.time.Minutes,
                    calculation.time.Seconds,
                    calculation.time.Milliseconds / 10
                );

                Color answerColor = (calculation.isCorrect ? Color.Green : Color.Red);

                
                createlabelAnswer(calculationQuestion, 2);
                createlabelAnswer("=");
                createlabelAnswer(calculationAnswer, answerColor, 2);

                //modifications if correct or not
                if (!calculation.isCorrect)
                {
                    createlabelAnswer(calculationCorrectAnswer,Color.Green, 2);
                    createlabelAnswer(calculationTime, 5);
                }
                else
                {
                    createlabelAnswer("", Color.Green, 2);
                    createlabelAnswer(calculationTime, 5);
                }
            }
        }

        /// <summary>
        /// Create label to display, general function
        /// </summary>
        /// <param name="textLabel">text to display</param>
        /// <param name="colorText">color of the text</param>
        /// <param name="columnSpan">columnspan in the parent (panelAnswersTalbeLayout)</param>
        /// <param name="rowSpan">rowspan in the parent (panelAnswersTalbeLayout)</param>
        public void createlabelAnswer(string textLabel, Color colorText, int columnSpan = 1, int rowSpan = 1)
        {
            Label labelAnswer = new Label();
            labelAnswer.Text = textLabel;
            labelAnswer.AutoSize = true;
            labelAnswer.Dock = DockStyle.Fill;
            labelAnswer.Font = new Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document, ((byte)(0)));
            labelAnswer.ForeColor = colorText;
            labelAnswer.Padding = new Padding(5);
            labelAnswer.TextAlign = ContentAlignment.MiddleLeft;

            // adding the label here to change it's rowSpan and columnSpan dynamically
            panelControls.Add(labelAnswer);
            panelAnswersTalbeLayout.SetRowSpan(labelAnswer, rowSpan);
            panelAnswersTalbeLayout.SetColumnSpan(labelAnswer, columnSpan);

        }

        /// <summary>
        /// alias of the general createlabelAnswer (only text)
        /// </summary>
        /// <param name="textLabel">text to display</param>
        public void createlabelAnswer(string textLabel)
        {
            createlabelAnswer(textLabel, Color.Black, 1, 1);
        }

        /// <summary>
        /// alias of the general createlabelAnswer (without color)
        /// </summary>
        /// <param name="textLabel">text to display</param>
        /// <param name="columnSpan">columnspan in the parent (panelAnswersTalbeLayout)</param>
        /// <param name="rowSpan">rowspan in the parent (panelAnswersTalbeLayout)</param>
        public void createlabelAnswer(string textLabel, int ColumnSpan = 1, int RowSpan = 1)
        {
            createlabelAnswer(textLabel, Color.Black, ColumnSpan, RowSpan);
        }
    }//end AnswersDisplay
}//end namespace
