﻿namespace Livrets
{
    partial class MainWindow
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMainTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonOptions = new System.Windows.Forms.Button();
            this.labelTimer = new System.Windows.Forms.Label();
            this.labelMode = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.panelMainContainer = new System.Windows.Forms.Panel();
            this.panelMainTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainTableLayout
            // 
            this.panelMainTableLayout.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelMainTableLayout.ColumnCount = 4;
            this.panelMainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelMainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelMainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelMainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelMainTableLayout.Controls.Add(this.buttonStop, 1, 2);
            this.panelMainTableLayout.Controls.Add(this.buttonOptions, 3, 2);
            this.panelMainTableLayout.Controls.Add(this.labelTimer, 3, 0);
            this.panelMainTableLayout.Controls.Add(this.labelMode, 0, 0);
            this.panelMainTableLayout.Controls.Add(this.buttonStart, 0, 2);
            this.panelMainTableLayout.Controls.Add(this.panelMainContainer, 0, 1);
            this.panelMainTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainTableLayout.Location = new System.Drawing.Point(0, 0);
            this.panelMainTableLayout.Name = "panelMainTableLayout";
            this.panelMainTableLayout.Padding = new System.Windows.Forms.Padding(10);
            this.panelMainTableLayout.RowCount = 3;
            this.panelMainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelMainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.panelMainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelMainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.panelMainTableLayout.Size = new System.Drawing.Size(784, 562);
            this.panelMainTableLayout.TabIndex = 1;
            // 
            // buttonStop
            // 
            this.buttonStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonStop.Location = new System.Drawing.Point(211, 505);
            this.buttonStop.Margin = new System.Windows.Forms.Padding(10, 8, 10, 8);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(171, 39);
            this.buttonStop.TabIndex = 6;
            this.buttonStop.TabStop = false;
            this.buttonStop.Text = "Arrêter";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Visible = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonOptions
            // 
            this.buttonOptions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonOptions.Location = new System.Drawing.Point(593, 505);
            this.buttonOptions.Margin = new System.Windows.Forms.Padding(10, 8, 10, 8);
            this.buttonOptions.Name = "buttonOptions";
            this.buttonOptions.Size = new System.Drawing.Size(171, 39);
            this.buttonOptions.TabIndex = 4;
            this.buttonOptions.Text = "Options";
            this.buttonOptions.UseVisualStyleBackColor = true;
            this.buttonOptions.Click += new System.EventHandler(this.buttonOptions_Click);
            // 
            // labelTimer
            // 
            this.labelTimer.AutoSize = true;
            this.labelTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document, ((byte)(0)));
            this.labelTimer.Location = new System.Drawing.Point(586, 10);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Padding = new System.Windows.Forms.Padding(5);
            this.labelTimer.Size = new System.Drawing.Size(185, 54);
            this.labelTimer.TabIndex = 1;
            this.labelTimer.Text = "00:00.00";
            this.labelTimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMode
            // 
            this.labelMode.AutoSize = true;
            this.labelMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document, ((byte)(0)));
            this.labelMode.Location = new System.Drawing.Point(13, 10);
            this.labelMode.Name = "labelMode";
            this.labelMode.Padding = new System.Windows.Forms.Padding(5);
            this.labelMode.Size = new System.Drawing.Size(185, 54);
            this.labelMode.TabIndex = 0;
            this.labelMode.Text = "Mode 1";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonStart
            // 
            this.buttonStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonStart.Location = new System.Drawing.Point(20, 505);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(10, 8, 10, 8);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(171, 39);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.TabStop = false;
            this.buttonStart.Text = "Nouvelle partie";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // panelMainContainer
            // 
            this.panelMainContainer.BackColor = System.Drawing.SystemColors.Control;
            this.panelMainContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelMainTableLayout.SetColumnSpan(this.panelMainContainer, 4);
            this.panelMainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainContainer.Location = new System.Drawing.Point(13, 67);
            this.panelMainContainer.Name = "panelMainContainer";
            this.panelMainContainer.Size = new System.Drawing.Size(758, 427);
            this.panelMainContainer.TabIndex = 5;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panelMainTableLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Livrets";
            this.panelMainTableLayout.ResumeLayout(false);
            this.panelMainTableLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel panelMainTableLayout;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Panel panelMainContainer;
        public System.Windows.Forms.Label labelTimer;
        public System.Windows.Forms.Button buttonStart;
        public System.Windows.Forms.Button buttonOptions;
        public System.Windows.Forms.Button buttonStop;



    }
}

