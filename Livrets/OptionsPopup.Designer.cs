﻿namespace Livrets
{
    partial class OptionsPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonConfirmOptions = new System.Windows.Forms.Button();
            this.buttonCancelOptions = new System.Windows.Forms.Button();
            this.groupBoxMode = new System.Windows.Forms.GroupBox();
            this.radioButtonMode3 = new System.Windows.Forms.RadioButton();
            this.radioButtonMode2 = new System.Windows.Forms.RadioButton();
            this.radioButtonMode1 = new System.Windows.Forms.RadioButton();
            this.groupBoxTable = new System.Windows.Forms.GroupBox();
            this.checkBoxFacrot15 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor14 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor13 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor12 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor11 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor10 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor9 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor8 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor7 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor6 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor5 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor4 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor3 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor2 = new System.Windows.Forms.CheckBox();
            this.checkBoxFactor1 = new System.Windows.Forms.CheckBox();
            this.groupBoxNbCalculus = new System.Windows.Forms.GroupBox();
            this.numericUpDownNbCalculus = new System.Windows.Forms.NumericUpDown();
            this.groupBoxDisplayTime = new System.Windows.Forms.GroupBox();
            this.numericUpDownDisplayTime = new System.Windows.Forms.NumericUpDown();
            this.tooltipOptions = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxMode.SuspendLayout();
            this.groupBoxTable.SuspendLayout();
            this.groupBoxNbCalculus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNbCalculus)).BeginInit();
            this.groupBoxDisplayTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisplayTime)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonConfirmOptions
            // 
            this.buttonConfirmOptions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirmOptions.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonConfirmOptions.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonConfirmOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonConfirmOptions.Location = new System.Drawing.Point(189, 220);
            this.buttonConfirmOptions.Margin = new System.Windows.Forms.Padding(5);
            this.buttonConfirmOptions.Name = "buttonConfirmOptions";
            this.buttonConfirmOptions.Size = new System.Drawing.Size(146, 46);
            this.buttonConfirmOptions.TabIndex = 0;
            this.buttonConfirmOptions.Text = "Valider";
            this.tooltipOptions.SetToolTip(this.buttonConfirmOptions, "Il faut qu\'au moins un Livret soit sélectionné pour valider les options");
            this.buttonConfirmOptions.UseVisualStyleBackColor = true;
            this.buttonConfirmOptions.Click += new System.EventHandler(this.buttonConfirmOptions_Click);
            // 
            // buttonCancelOptions
            // 
            this.buttonCancelOptions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCancelOptions.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelOptions.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCancelOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonCancelOptions.Location = new System.Drawing.Point(388, 220);
            this.buttonCancelOptions.Margin = new System.Windows.Forms.Padding(5);
            this.buttonCancelOptions.Name = "buttonCancelOptions";
            this.buttonCancelOptions.Size = new System.Drawing.Size(146, 46);
            this.buttonCancelOptions.TabIndex = 1;
            this.buttonCancelOptions.Text = "Annuler";
            this.buttonCancelOptions.UseVisualStyleBackColor = true;
            // 
            // groupBoxMode
            // 
            this.groupBoxMode.Controls.Add(this.radioButtonMode3);
            this.groupBoxMode.Controls.Add(this.radioButtonMode2);
            this.groupBoxMode.Controls.Add(this.radioButtonMode1);
            this.groupBoxMode.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBoxMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.groupBoxMode.Location = new System.Drawing.Point(25, 24);
            this.groupBoxMode.Name = "groupBoxMode";
            this.groupBoxMode.Size = new System.Drawing.Size(200, 188);
            this.groupBoxMode.TabIndex = 3;
            this.groupBoxMode.TabStop = false;
            this.groupBoxMode.Text = "Mode";
            // 
            // radioButtonMode3
            // 
            this.radioButtonMode3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonMode3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButtonMode3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonMode3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.radioButtonMode3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButtonMode3.Location = new System.Drawing.Point(14, 132);
            this.radioButtonMode3.Margin = new System.Windows.Forms.Padding(0);
            this.radioButtonMode3.Name = "radioButtonMode3";
            this.radioButtonMode3.Size = new System.Drawing.Size(172, 41);
            this.radioButtonMode3.TabIndex = 4;
            this.radioButtonMode3.TabStop = true;
            this.radioButtonMode3.Text = "Mode 3";
            this.radioButtonMode3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tooltipOptions.SetToolTip(this.radioButtonMode3, "Fonctionne comme le mode 1 avec saisie dans l\'application et résultats");
            this.radioButtonMode3.UseVisualStyleBackColor = true;
            // 
            // radioButtonMode2
            // 
            this.radioButtonMode2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonMode2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButtonMode2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonMode2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.radioButtonMode2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButtonMode2.Location = new System.Drawing.Point(14, 85);
            this.radioButtonMode2.Margin = new System.Windows.Forms.Padding(0);
            this.radioButtonMode2.Name = "radioButtonMode2";
            this.radioButtonMode2.Size = new System.Drawing.Size(172, 41);
            this.radioButtonMode2.TabIndex = 3;
            this.radioButtonMode2.TabStop = true;
            this.radioButtonMode2.Text = "Mode 2";
            this.radioButtonMode2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tooltipOptions.SetToolTip(this.radioButtonMode2, "Affiche des calculs de livrets de la forme 5 x ??? = 25");
            this.radioButtonMode2.UseVisualStyleBackColor = true;
            // 
            // radioButtonMode1
            // 
            this.radioButtonMode1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonMode1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButtonMode1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonMode1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.radioButtonMode1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButtonMode1.Location = new System.Drawing.Point(14, 38);
            this.radioButtonMode1.Margin = new System.Windows.Forms.Padding(0);
            this.radioButtonMode1.Name = "radioButtonMode1";
            this.radioButtonMode1.Size = new System.Drawing.Size(172, 41);
            this.radioButtonMode1.TabIndex = 0;
            this.radioButtonMode1.TabStop = true;
            this.radioButtonMode1.Text = "Mode 1";
            this.radioButtonMode1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tooltipOptions.SetToolTip(this.radioButtonMode1, "Affiche des calculs de livrets sous la forme 5 x 3 = ???");
            this.radioButtonMode1.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable
            // 
            this.groupBoxTable.Controls.Add(this.checkBoxFacrot15);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor14);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor13);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor12);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor11);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor10);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor9);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor8);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor7);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor6);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor5);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor4);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor3);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor2);
            this.groupBoxTable.Controls.Add(this.checkBoxFactor1);
            this.groupBoxTable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBoxTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.groupBoxTable.Location = new System.Drawing.Point(231, 24);
            this.groupBoxTable.Name = "groupBoxTable";
            this.groupBoxTable.Size = new System.Drawing.Size(262, 188);
            this.groupBoxTable.TabIndex = 4;
            this.groupBoxTable.TabStop = false;
            this.groupBoxTable.Text = "Livrets";
            // 
            // checkBoxFacrot15
            // 
            this.checkBoxFacrot15.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFacrot15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFacrot15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFacrot15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFacrot15.Location = new System.Drawing.Point(204, 132);
            this.checkBoxFacrot15.Name = "checkBoxFacrot15";
            this.checkBoxFacrot15.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFacrot15.TabIndex = 14;
            this.checkBoxFacrot15.Text = "15";
            this.checkBoxFacrot15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFacrot15.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor14
            // 
            this.checkBoxFactor14.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor14.Location = new System.Drawing.Point(157, 132);
            this.checkBoxFactor14.Name = "checkBoxFactor14";
            this.checkBoxFactor14.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor14.TabIndex = 13;
            this.checkBoxFactor14.Text = "14";
            this.checkBoxFactor14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor14.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor13
            // 
            this.checkBoxFactor13.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor13.Location = new System.Drawing.Point(110, 132);
            this.checkBoxFactor13.Name = "checkBoxFactor13";
            this.checkBoxFactor13.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor13.TabIndex = 12;
            this.checkBoxFactor13.Text = "13";
            this.checkBoxFactor13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor13.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor12
            // 
            this.checkBoxFactor12.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor12.Location = new System.Drawing.Point(63, 132);
            this.checkBoxFactor12.Name = "checkBoxFactor12";
            this.checkBoxFactor12.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor12.TabIndex = 11;
            this.checkBoxFactor12.Text = "12";
            this.checkBoxFactor12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor12.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor11
            // 
            this.checkBoxFactor11.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor11.Location = new System.Drawing.Point(16, 132);
            this.checkBoxFactor11.Name = "checkBoxFactor11";
            this.checkBoxFactor11.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor11.TabIndex = 10;
            this.checkBoxFactor11.Text = "11";
            this.checkBoxFactor11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor11.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor10
            // 
            this.checkBoxFactor10.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor10.Location = new System.Drawing.Point(204, 85);
            this.checkBoxFactor10.Name = "checkBoxFactor10";
            this.checkBoxFactor10.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor10.TabIndex = 9;
            this.checkBoxFactor10.Text = "10";
            this.checkBoxFactor10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor10.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor9
            // 
            this.checkBoxFactor9.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor9.Location = new System.Drawing.Point(157, 85);
            this.checkBoxFactor9.Name = "checkBoxFactor9";
            this.checkBoxFactor9.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor9.TabIndex = 8;
            this.checkBoxFactor9.Text = "9";
            this.checkBoxFactor9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor9.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor8
            // 
            this.checkBoxFactor8.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor8.Location = new System.Drawing.Point(110, 85);
            this.checkBoxFactor8.Name = "checkBoxFactor8";
            this.checkBoxFactor8.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor8.TabIndex = 7;
            this.checkBoxFactor8.Text = "8";
            this.checkBoxFactor8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor8.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor7
            // 
            this.checkBoxFactor7.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor7.Location = new System.Drawing.Point(63, 85);
            this.checkBoxFactor7.Name = "checkBoxFactor7";
            this.checkBoxFactor7.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor7.TabIndex = 6;
            this.checkBoxFactor7.Text = "7";
            this.checkBoxFactor7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor7.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor6
            // 
            this.checkBoxFactor6.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor6.Location = new System.Drawing.Point(16, 85);
            this.checkBoxFactor6.Name = "checkBoxFactor6";
            this.checkBoxFactor6.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor6.TabIndex = 5;
            this.checkBoxFactor6.Text = "6";
            this.checkBoxFactor6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor6.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor5
            // 
            this.checkBoxFactor5.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor5.Location = new System.Drawing.Point(204, 38);
            this.checkBoxFactor5.Name = "checkBoxFactor5";
            this.checkBoxFactor5.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor5.TabIndex = 4;
            this.checkBoxFactor5.Text = "5";
            this.checkBoxFactor5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor5.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor4
            // 
            this.checkBoxFactor4.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor4.Location = new System.Drawing.Point(157, 38);
            this.checkBoxFactor4.Name = "checkBoxFactor4";
            this.checkBoxFactor4.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor4.TabIndex = 3;
            this.checkBoxFactor4.Text = "4";
            this.checkBoxFactor4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor4.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor3
            // 
            this.checkBoxFactor3.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor3.Location = new System.Drawing.Point(110, 38);
            this.checkBoxFactor3.Name = "checkBoxFactor3";
            this.checkBoxFactor3.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor3.TabIndex = 2;
            this.checkBoxFactor3.Text = "3";
            this.checkBoxFactor3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor3.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor2
            // 
            this.checkBoxFactor2.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor2.Location = new System.Drawing.Point(63, 38);
            this.checkBoxFactor2.Name = "checkBoxFactor2";
            this.checkBoxFactor2.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor2.TabIndex = 1;
            this.checkBoxFactor2.Text = "2";
            this.checkBoxFactor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor2.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactor1
            // 
            this.checkBoxFactor1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxFactor1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxFactor1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxFactor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.checkBoxFactor1.Location = new System.Drawing.Point(16, 38);
            this.checkBoxFactor1.Name = "checkBoxFactor1";
            this.checkBoxFactor1.Size = new System.Drawing.Size(41, 41);
            this.checkBoxFactor1.TabIndex = 0;
            this.checkBoxFactor1.Text = "1";
            this.checkBoxFactor1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxFactor1.UseVisualStyleBackColor = true;
            // 
            // groupBoxNbCalculus
            // 
            this.groupBoxNbCalculus.Controls.Add(this.numericUpDownNbCalculus);
            this.groupBoxNbCalculus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBoxNbCalculus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBoxNbCalculus.Location = new System.Drawing.Point(499, 24);
            this.groupBoxNbCalculus.Name = "groupBoxNbCalculus";
            this.groupBoxNbCalculus.Size = new System.Drawing.Size(200, 91);
            this.groupBoxNbCalculus.TabIndex = 4;
            this.groupBoxNbCalculus.TabStop = false;
            this.groupBoxNbCalculus.Text = "Nombre de calculs";
            // 
            // numericUpDownNbCalculus
            // 
            this.numericUpDownNbCalculus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.numericUpDownNbCalculus.Location = new System.Drawing.Point(19, 44);
            this.numericUpDownNbCalculus.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownNbCalculus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNbCalculus.Name = "numericUpDownNbCalculus";
            this.numericUpDownNbCalculus.Size = new System.Drawing.Size(136, 29);
            this.numericUpDownNbCalculus.TabIndex = 0;
            this.numericUpDownNbCalculus.Tag = "";
            this.numericUpDownNbCalculus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownNbCalculus.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBoxDisplayTime
            // 
            this.groupBoxDisplayTime.Controls.Add(this.numericUpDownDisplayTime);
            this.groupBoxDisplayTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBoxDisplayTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBoxDisplayTime.Location = new System.Drawing.Point(499, 121);
            this.groupBoxDisplayTime.Name = "groupBoxDisplayTime";
            this.groupBoxDisplayTime.Size = new System.Drawing.Size(200, 91);
            this.groupBoxDisplayTime.TabIndex = 5;
            this.groupBoxDisplayTime.TabStop = false;
            this.groupBoxDisplayTime.Text = "Temps d\'affichage [s]";
            // 
            // numericUpDownDisplayTime
            // 
            this.numericUpDownDisplayTime.Location = new System.Drawing.Point(19, 41);
            this.numericUpDownDisplayTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDisplayTime.Name = "numericUpDownDisplayTime";
            this.numericUpDownDisplayTime.Size = new System.Drawing.Size(136, 29);
            this.numericUpDownDisplayTime.TabIndex = 1;
            this.numericUpDownDisplayTime.Tag = "";
            this.numericUpDownDisplayTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownDisplayTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tooltipOptions
            // 
            this.tooltipOptions.AutoPopDelay = 5000;
            this.tooltipOptions.InitialDelay = 500;
            this.tooltipOptions.ReshowDelay = 100;
            // 
            // OptionsPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 278);
            this.Controls.Add(this.groupBoxDisplayTime);
            this.Controls.Add(this.groupBoxNbCalculus);
            this.Controls.Add(this.groupBoxTable);
            this.Controls.Add(this.groupBoxMode);
            this.Controls.Add(this.buttonCancelOptions);
            this.Controls.Add(this.buttonConfirmOptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OptionsPopup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Options";
            this.groupBoxMode.ResumeLayout(false);
            this.groupBoxTable.ResumeLayout(false);
            this.groupBoxNbCalculus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNbCalculus)).EndInit();
            this.groupBoxDisplayTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisplayTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConfirmOptions;
        private System.Windows.Forms.Button buttonCancelOptions;
        private System.Windows.Forms.GroupBox groupBoxMode;
        private System.Windows.Forms.RadioButton radioButtonMode1;
        private System.Windows.Forms.GroupBox groupBoxTable;
        private System.Windows.Forms.CheckBox checkBoxFactor1;
        private System.Windows.Forms.CheckBox checkBoxFactor5;
        private System.Windows.Forms.CheckBox checkBoxFactor4;
        private System.Windows.Forms.CheckBox checkBoxFactor3;
        private System.Windows.Forms.CheckBox checkBoxFactor2;
        private System.Windows.Forms.CheckBox checkBoxFacrot15;
        private System.Windows.Forms.CheckBox checkBoxFactor14;
        private System.Windows.Forms.CheckBox checkBoxFactor13;
        private System.Windows.Forms.CheckBox checkBoxFactor12;
        private System.Windows.Forms.CheckBox checkBoxFactor11;
        private System.Windows.Forms.CheckBox checkBoxFactor10;
        private System.Windows.Forms.CheckBox checkBoxFactor9;
        private System.Windows.Forms.CheckBox checkBoxFactor8;
        private System.Windows.Forms.CheckBox checkBoxFactor7;
        private System.Windows.Forms.CheckBox checkBoxFactor6;
        private System.Windows.Forms.GroupBox groupBoxNbCalculus;
        private System.Windows.Forms.GroupBox groupBoxDisplayTime;
        private System.Windows.Forms.NumericUpDown numericUpDownNbCalculus;
        private System.Windows.Forms.NumericUpDown numericUpDownDisplayTime;
        private System.Windows.Forms.ToolTip tooltipOptions;
        private System.Windows.Forms.RadioButton radioButtonMode3;
        private System.Windows.Forms.RadioButton radioButtonMode2;


    }
}