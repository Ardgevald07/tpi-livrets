﻿///ETML 
///Author : Miguel Pombo Dias
///Summary : Class to create an object of a calculation and it's results
///Git repository : https://bitbucket.org/Ardgevald07/tpi-livrets

using System;

namespace Livrets
{
    /// <summary>
    /// Calculation object containing all the managing of a calculation
    /// and granting easy access to all its values
    /// </summary>
    public class Calculation : IComparable
    {
        // same as interface, but internal values
        private int _baseFactor;
        private int _tableFactor;
        private TimeSpan _time;
        private string _answer;
        private bool _isCorrect;

        // assigning factors at creation
        public Calculation(int baseFactor, int tableFactor)
        {
            _baseFactor = baseFactor;
            _tableFactor = tableFactor;
            _time = TimeSpan.Zero;
            _answer = "___";
        }

        /// <summary>
        /// get the base factor (always 1 to 15)
        /// </summary>
        public int baseFactor
        {
            get { return _baseFactor; }
        }

        /// <summary>
        /// get the table factor (chosen by user)
        /// </summary>
        public int tableFactor
        {
            get { return _tableFactor; }
        }

        /// <summary>
        /// get and set the time used to answer the calculation
        /// </summary>
        public TimeSpan time
        {
            get { return _time; }

            set { _time = value; }
        }

        /// <summary>
        /// get and set the answer to the calculation
        /// assign the isCorrect value
        /// </summary>
        public string answer
        {
            get { return _answer; }

            set
            {
                _answer = value;
                int intAnswer;

                if (int.TryParse(value, out intAnswer))
                {
                    _isCorrect = (_baseFactor * _tableFactor == intAnswer);
                }
                else
                {
                    _isCorrect = false;
                }
            }
        }

        /// <summary>
        /// get if the answer is correct
        /// </summary>
        public bool isCorrect
        {
            get { return _isCorrect; }
        }

        /// <summary>
        /// Use IComparable interface to use in sorting
        /// </summary>
        /// <param name="objectToCompare">object compared</param>
        /// <returns>value to compare for sorting</returns>
        /// <exception cref="ArgumentException">bad object type</exception>
        public int CompareTo(object objectToCompare)
        {
            if (objectToCompare is Calculation)
            {
                return this.time.TotalMilliseconds.CompareTo(
                    (objectToCompare as Calculation).time.TotalMilliseconds
                );
            }
            throw new ArgumentException("Object is not a Calculation");
        }
    }// end class Calculation
}//end namespace
