﻿

using System.Windows.Forms;

namespace Livrets
{
    public partial class CalculationDisplay : UserControl
    {

        public CalculationDisplay()
        {
            InitializeComponent();
        }

        /// <summary>
        /// set the calculus label
        /// </summary>
        /// <param name="calculusText">calculus text</param>
        /// <param name="stepText">step text we are at</param>
        public void SetLabelsCalculus(string calculusText, string stepText)
        {
            //delegation if needed
            if (this.labelCalculation.InvokeRequired)
            {

                BeginInvoke((MethodInvoker)delegate()
                {
                    labelCalculation.Text = calculusText;
                    labelNumberCalculus.Text = stepText;
                });
            }
            else
            {
                labelCalculation.Text = calculusText;
                labelNumberCalculus.Text = stepText;
            }
        }

        /// <summary>
        /// Sets the remaining time (each second)
        /// </summary>
        /// <param name="remainingTime">time remaining (already formatted)</param>
        public void SetRemainingTime(string remainingTime)
        {
            if (this.labelRemainingTime.InvokeRequired)
            {

                labelRemainingTime.BeginInvoke((MethodInvoker)delegate()
                {
                    labelRemainingTime.Text = remainingTime;
                });
            }
            else
            {
                labelRemainingTime.Text = remainingTime;
            }
        }
    }//end CalculationDisplay
}//end namespace
